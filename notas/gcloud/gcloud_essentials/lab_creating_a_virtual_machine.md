##

```
# List the active account name
gcloud auth list

# Listing the project id
gcloud config list project


## Setando região para us-west1
gcloud config set compute/region us-west1

### Criando um e2-medium na zona da variavel ZONE
gcloud compute instances create gcelab2 --machine-type e2-medium --zone=$ZONE

### Listando instancias via gcloud cli
gcloud compute instances list

### Help para criar instancias(VMS)
gcloud compute instances create --help


## removendo instancia
gcloud compute instances delete $NAME --zone=$ZONE


```
