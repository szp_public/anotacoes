# Kubernetes Engine: Qwik Start

GSP100: https://www.cloudskillsboost.google/focuses/878?parent=catalog


## Cluster creation

```
#Criando cluster nomeado lab-cluster
gcloud container clusters create --machine-type=e2-medium --zone=$ZONE lab-cluster

# listando cluster pelo nome
gcloud container clusters list --filter=NAME:'lab-cluster'


#Autenticando-se no cluster `lab-cluster` para habilitar o kubectl
gcloud container clusters get-credentials lab-cluster


```

## Criando deployment/serviço

```
#Criando deployment
kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0

#Expondo deployment
kubectl expose deployment hello-server --type=LoadBalancer --port 8080
```
