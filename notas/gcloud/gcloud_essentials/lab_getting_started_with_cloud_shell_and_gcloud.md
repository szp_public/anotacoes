# Getting Started with Cloud Shell and gcloud

GSP002: https://www.cloudskillsboost.google/focuses/563?parent=catalog


## Referência
https://cloud.google.com/sdk/gcloud/reference

1. Configs e instâncias
```
#listando valor de um recurso
gcloud config get-value <RESOURCE>

#Ajustando valor de um recurso
gcloud config set <RECURSO> <VALOR>

#Descrevendo projeto
gcloud compute project-info describe --project $(gcloud config get-value project)


#Criando VM
gcloud compute instance create <NOME> --zone=<ZONA> --machine-type=<Tipo da instancia>


```

## Tagging

```
#Adicionando tags a uma instância
gcloud compute instances add-tags gcelab2 --tags http-server,https-server
```


## Filtragem

```
# Filtrando instância pelo nome
gcloud compute instance list --filter="name=('gcelab2')"

# Filtrando regras de firewall
gcloud compute firewall-rules list --filter="NETWORK:'default'"
# Segunda forma
gcloud compute firewall-rules list --filter="network='default'"

# Filtragem com operador AND
gcloud compute firewall-rules list --filter="NETWORK:'default' AND ALLOW:'icmp'"


#Criando regra de ALLOW para porta 80(http) na tag https-server
gcloud compute firewall-rules create default-allow-http --action=ALLOW --direction=INGRESS --priority=1000 --rules=tcp:80 source-ranges=0.0.0.0/0 --target-tags=http-server


# Listando regra acrescentada
gcloud compute firewall-rules list --filter='ALLOW:80'

# testando conexão http dentro da console gcloud
curl http://$(gcloud compute instances list --filter=name:gcelab2 --format='value(EXTERNAL_IP)')

```

## System logs
```
#Listando tipos de logs
gcloud logging logs list

#Filtrando logs pela palavra 'compute'
gcloud logging logs list --filter="compute"


# Lendo logs pelo tipo 'gce_instance'
gcloud logging logs list --filter='resource.type=gce_instance' --limit 5


# Lendo logs pelo tipo gce_instance e vm com nome gcelab2

gcloud logging logs list --filter='resource.type=gce_instance AND labels.instance_name='gcelab2' --limit 5
```


