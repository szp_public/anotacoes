## VPC

+ Quota máxima de 15 redes
+ É possível dividir em subnets
+ Cada subnet tem 4 IPs reservados, ou seja, um /29 aloca 8 IPs, mas somente 4 são utilizáveis para instâncias, pois os outros 4 são reservados
+ 

## IP addr

+ Internal
    * Alocado a partir do range da subnet via DHCP interno
    * DHCP Lease: 24h
    * vm_name+ip registrado no dns interno da rede
        - FQDN: [hostname].[zone].c.[project-id].internal
    * Toda VM ganha IP
    * Todo serviço que depende de VM ganha IP. Ex: APP Engine
    * É possível reservar o IP
    * Cada instância tem um *metadata server* que também age como DNS resolver
    * Metadata server
        - Resolve DNS internos
        - Roteia queries externas para public Google DNS
    * Existe uma tabela de lookup de resolução de External IP para Internal IP para instâncias relevantes

+ External
    * Pode ser A partir de um pool -> dinâmico
    * Pode ser Reserved (static)
    * Pode ser Bring Your Own IP addres (BYOIP) -> somente /24 ou maior
    * VM OS não conhece o próprio IP externo 
    * São mapeados para IPs internos
    * Registros públicos de DNS não são automaticamente publicados, caso necessário admins devem fazê-lo ao usar servidores de DNS existentes. Considere o uso do serviço Cloud DNS

### Alias IP Ranges    
+  Serviço que permite associar um range de IPs como *alias* para uma interface de rede da VM
    - Quase parecido com uma *placa de rede virtual*, mas com um CIDR range


### Cloud Routes

+ Sempre tem uma rota padrão para fora da rede
+ Uma rota é criada sempre que uma subnet é criada
+ Uma rota é aplicada a uma instância se a rede e as tags "casarem"
    * Se a rede "bate" mas com ausência de tags  especificadas, a rota será aplicada a todas as instâncias


### Cloud firewall

+ Essencialmente, cada VPC funciona como um firewall distribuído
+ Regras de firewall são aplicadas na rede como um todo, mas as conexões são permitidas/negadas a nível de instância
    * Entre instâncias da mesma rede
+ Egress -> Allow All implícito
+ Ingress -> Deny all implícito
+ Regras de firewall são compostas de:
    * direction
    * Source or Destination
        - Ingress: IP, source tags ou sorce service account
        - Egress: IP range(s)
    * Protocol and Port
    * Action
    * Priority
    * Rule assignment: Todos ou certas regras para certas instâncias



### Precificação

+ Rede
    * Ingress -> Gratuito
    * Egress IP interno -> Gratuito
    * Egress produtos Google -> Gratuito
    * Alguns Egress's para produtos Google Cloud na mesma região -> Gratuito
+ IP externo 
    * Cobrado tanto para estático quanto dinâmico
    * Exceto se tiver associado a *forwarding rules*

### Design

+ É boa prática desabilitar IP externo para instâncias, deixando somente IP interno
+ Recomendado utilizar o serviço *Cloud NAT* para que as instâncias acesses recursos externos (ex: Internet)
    * Inbound NAT -> NAT com mapeamento de entrada
    * Outbound NAT -> NAT com mapeamento de saída
+ Private Google Access (PGA) deve ser configurado para que as instâncias (com IP Interno somente) consigam comunicar-se com serviços e APIs Google (External IP)
    * Ex.: Se uma VM precisa acessar uma Cloud Storage, o PGA deve ser habilitado para que isso seja possível
    * Ativado de subrede para subrede
    * Não surte efeito em instâncias que tem IP público, pois o tráfego sairá pela internet


## VM

+ As VMs são dividias em classes. 
    * Ex.: E2 possui shared cores (vCpus) e custam mais baixo
+ Network tags servem para associar intâncias às regras de firewall
+ É possível montar vários tipos de discos
    * Os discos são montados via conexão de rede e não fisicamente
    * Há uma modalidade de montagem de disco local, porém há limitações e preços diferenciados
+ A Imagem padrão Debian já tem o gcloud cli incorporado
+ Ao criar VM's, é possível definir acessos específicos às API's do Google
    * No exemplo do lab, é configurado o acesso R+W ao Cloud Storage 
+ Scripts de startup e shutdown são configurados via K,V com as seguintes Ks na seção *metadata*:
    * startup: startup-script-url
    * shutdown: shutdown-script-url	
+ Os valores V dos scripts são URL's
+ Os discos anexados são encriptados
    * Google Managed key
    * Customer managed key
    * Customer supplied key
