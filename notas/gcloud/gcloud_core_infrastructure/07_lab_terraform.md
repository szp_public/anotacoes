# Automating the Deployment of Infrastructure Using Terraform

## Cheatsheet

```shell

## create folder
mkdir tfinfra && cd tfinfra

#create provider

tee provider.tf <<EOF
provider "google" {}
EOF


## Initialize
terraform init


#format
terraform fmt

# create execution plan
terraform plan

# apply configs idempotently
terraform  apply


```