# Hello Cloud Run [APPRUN]

## cheatsheet

```shell

# Enabling Cloud Run
gcloud services enable run.googleapis.com

# Build dockerfile and push to remote
gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld


# Listing images
gcloud container images list

# Running pushed image

docker run -d -p 8080:8080 gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld

# Note: If the docker command cannot pull the remote container image then try running this
gcloud auth configure-docker

# Deploy
gcloud run deploy --image gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld --allow-unauthenticated --region=$LOCATION

# Cleanup

## Deleting images
glcoud container images delete gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld 

## Delete Cloud Run Service
gcloud run services delete helloworld --region=$LOCATION

```