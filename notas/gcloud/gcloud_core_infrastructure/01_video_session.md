# Google Cloud Fundamentals: Core Infrastructure

## Intro

## Resources and Access


## Virtual machines and Networks


### VPC

+ possui tabela de roteamento embutida, ou seja, gerenciadas diretamente pelo Google
    - roteamento para redes, subredes e zonas sem necessidade de haver IP externo
+ Para conectar VPCs de projetos diferentes:
    - Relacionamento via Peering
    - Shared VPC via IAM 
    - VPC tem firewall embutido (firewall rules)
    - São globais 
    - Uma VPC pode estar configurada em várias zonas de uma região
    - A comunicação entre VMs na mesma subnet são bloqueadas por padrão
    - Não suporta multicast/broadcast
    - Regras de firewall podem ser atreladas a **tags** das instâncias. Ex.: Liberar tráfego na TCP:443 para todas as aplicações com tag "webserver-https"
    - Subnets podem ser separadas por região
    - subnets abrangem as zonas da região
+ VPC é um recurso. Então está vinculada a um projeto
> Recomendação:
> 
>    É recomendando criar large subnets por região por conta da simplicidade

Mais em: https://cloud.google.com/architecture/deploy-hub-spoke-vpc-network-topology
    

### Firewall rules
+ Ingress bloqueado por padrão
+ Egress liberado por padrão
+ Pode ser associada a instance  tags ou service accounts
+ Quando associada a uma SA, é necessário um restart nas VM's para ter efeito


### Firewall Policies
+ Engloba uma ou mais firewall rules
+ Rules são processadas por ordem, a ordem é definida pelo atributo prioridade (menor valor = +prioritário)
+ policies podem ser associada à secure tags
+ A secure tag fica disponível na org inteira
+ https://cloud.google.com/architecture/building-internet-connectivity-for-private-vms


### Compute Engine 

+ Instâncias reservadas podem economizar até 57%, com comprometimento de uso estendido
+ Instâncias "Spot":
    - Preemptive - Opera por no máximo 24h
    - Spot - Não tem tempo máximo de execução
    - Pode ser parado a qualquer momento caso algum outro processo requira mais recursos 
+ 

### Load balancing

+ É uma configuração e não roda em VM, evitando necessidade de escalar os LBs
+ Balanceia
    - HTTP/HTTPS
    - UDP
    - TCP
    - SSL/TCP
+ Modelos:
    - Global HTTP(s) load balancing: crossregional LB
    - SSL/TCP: Global SSL Proxy Load Balancer (somente algumas portas)
    - TCP: Global TCP Proxy LB (somente algumas portas)
    - Regional Internal Load Balancer: Garante que todos os clientes e back-ends sejam de uma região específica. Baseado em HTTP(s)

Veja mais: https://cloud.google.com/load-balancing/docs/l7-internal

### Cloud DNS & CDN

+ Estratégia de edge caching para armazenar conteúdo/cache mais próximo dos usuários
+ Ao habilidar Cloud CDN (depois de configurar o LB), o usuário buscará o conteúdo mais próximo geograficamente


### Connecting networks to Google VPC


+ Meios de conectar a outras redes (privadas, outros cloud providers):
    * VPN/IPSec (Internet)
        - Uso de Cloud router
            - Informações de rotas propagadas via Border Gateway Protocol (BGP), quando adicionar uma nova subnet, as rotas serão automaticamente configuradas
    * Direct Peering: Alocar um roteador no mesmo datacenter público como um Google Point of Presence (Google POP), para trafegar dados entre redes
        - Google tem 100+ POPs pelo mundo
    * Carrier Peering: Acesso direto da rede onprem ao Google Workspace e Google Cloud por meio de um provedor de serviços (service provider) - Não está sujeito a SLA
    * Dedicated Interconnect - Equivalente a "puxar uma fibra" direto com a Google Cloud
    * Partner Interconnect - Usar um service provider que suporte este tipo de conexão

### Google Cloud resource hierarchy

+ É organizado da seguinte forma:
```
+ organization node
    |
    |-- folder a
    |-- folder b
    |-- folder c
    |-- folder d
    |---|---project 1
    |---|---project 2
    |---|---|-- resource (compute engie)
    |---|---|--- resource (cloud storage)
```
Onde:
+ As policies são aplicadas hierarquicamente
+ folders podem ter subfolders
+ resources são onde os serviços são alocados para cada projeto
+ Não é possível definir folders sem organization nodes
+ Organization nodes são opcionais
+ É possível ter mais de uma organization node
+ Ex.:
    * Org: Empresa Acme
        - Folder 01: Jurídico
            - Resource A01
            - Resource X01
        - Folder 02: Financeiro
            - Resource B01
            - Resource C01
        - Folder 03: Recursos Humanos
            - Resource Y01
            - Resource A02
            - Resource X02
### IAM

+ Identity and Access Management
+ Serve para organizar as políticas de acesso aos recursos
+ O termo "Principal" é utilizado para identificar o "Quem", as políticas IAM podem ser definidas para: 
    - Service accounts
    - Google group
    - Google Accounts
    - Cloud Identity Domain
+ O IAM é RBAC, porem granular
+ Tipos de roles:
    - básicos
    - Pré-definidos
    - Custom roles: É a forma granular
        * Só pode ser aplicado nos níveis de Project e Organization


### Service accounts

+ Tipo de conta associada a recursos e serviços
    - Ex.: Uma CE usa uma SA para acessar uma Cloud Storage
+ Usa chave criptográfica para acessar recursos
+ Limite de uma SA por VM
+ Recomendado o uso de SA devido ao princípio do Least privilege access (LPA)
+ É possível associar IAM Policies a uma SA
    - Ex.: Usuario A01 tem permissão de *viewer* na SA01, enquanto usuário B01 tem permissão de *editor* na SA01


### Cloud identity

+ Mecanismo de centralização e gerenciamento de identidade
+ Provê meio facilitado de gerenciar as identidades e principals associadas.
    - Ex.: Quando um usuário deixa a empresa, é facilitadaa forma de remoção de acesso deste usuário
+ Integra-se com AD/LDAP


### Formas de interação com a Gcloud

+ Cloud Console: Web based
+ Cloud SDK and Cloud Shell: glcoud cli + sdk/vm associada;
+ APIs: REST,gRPC e Libs de algumas linguagens (Java, C#, Python, Node.js, PHP, Go, C++, Ruby)
+ Cloud Console Mobile App
    - Pode, por exemplo, acessar shell da CE via SSH
    - Várias outras ações


## Storage in  the cloud


5 tipos abordados na quest:
- Cloud Storage
- Cloud SQL
- Cloud Spanner
- Firestore
- Cloud BigTable

### Cloud Storage

+ Serviço de amazenamento de objetos
+ Objetos são armazenados em formato binário juntamento com metadados: `bin+metatada`
+ As chaves são no formato de URL
+ organizados em buckets
    - Região geográfica
    - Nome único
+ Possível habilitar versionamento (vide git)
    - sem versionamento, versões novas dos objetos sempre sobrescrevem as antigas
+ Controle de acesso via:
    - IAM roles
    - ACL
+ Possui controle de ciclo de vida de um objeto. Ex:
    - Expiração de objeto após determinado tempo
    - Manter apenas um número específico de versões de um objeto
+ 4 classes primárias de armazenamento:
    - Standard Storage: 
        * melhor para *hot data*, eg, acessados mais frequentemente
        * Ideal para objetos que serão armazenados por um período curto de tempo (temporário)
    - Nearline Storage
        * Ideal para dados infrequentes (ex.: acesso 1x por mes ou menos)
    - Coldline Storage:
        * menor custo de armazenamento
        * Ideal para objetos que são r/w, no máximo, a cada 90d
    - Archive Storage:
        * Atualmente é a opção mais barata de armazenamento
        * Ideal para dados de DR, arquivamento, backup online (acesso menor que 1x ao ano)
        + O custo de acesso aos arquivos é maior e o tempo de arquivamento é de, no mínimo, 365d
+ Existe uma serviço de transferência de arquivos chamado Storage Transfer Service para transferir arquivos das fontes :
    - HTTP(S) Endpoint
    - outros Cloud Providers
    - Outra região da GCLOUD
* Existe um serviço rackable (montávem em racks) para transferência de arquivos chamado *Transfer Appliance*:
    - rackable
    - *Alugável* da Google cloud
    - basicamente vc conecta na rede interna, transfere para este appliance e envia este para uma localidade GCloud
    - Até 1 PB de dados

### Cloud SQL

+ Serviço de bancos relacionais:
    - PostgreSQL
    - SQL Server
    - MySQL
    - etc

+ Abstrai operações, deixando o foco do cliente no dev de apps

### Cloud Spanner
+ Banco relacional da GCloud
+ Escalabilidade horizontal (repl)
+ SQL queries

### Firestore
+ NoSQL DB para mobile, web e server
+ Document based, organizado em collections 
+ POssui sistema de sync de dados com dispositivos conectados
+ Cobrança por cada r/w/d no Firestore
+ Existe uma quota diária gratuita, a cobrança começa somente quando esta cota se esgota


### Cloud BigTable
+ NoSQL big data db
+ Mesmo serviço utilizado por:
    - Google Analyltics
    - Google Search
    - Google Maps
    - Gmail
+ Formas de integração via API:
    - Managed VMs
    - HBase REST Server
    - Java Server com HBase client
+ Data Streaming:
    - STOMP
    - Spark Streaming
    - Dataflow Streaming
+ Batch (read):
    - Hadoop
    - MapReduce
    - Dataflow
    - Spark


## App Engine

+ Serviço servless para rodar containers
+ Suporta:
    - Eclipse, IntelliJ, Maven, Git, Jenkins, and PyCharm.
+ Serviços embutidos:
    - APIs
     - NoSQL Datastores
     - Memcache LB
     - Application Logging
     - User Auth API
+ Oferece SDK
+ Google Security Command Center
    - Provê segurança de aplicações
    - Faz scan em busca das principais vulnerabilidades

+ 2 tipos de ambiente:
    - Standard
        * Container based
        * Preconfigurado com uma lista de linguagens suportadas
            - Travado pela plataforma
        * Deploy feito via SDK
        * Possui persistent storage
        * Autoscaling
        * Roda em sandbox
    - Flexible
        * Possível especificar o tipo de container (Docker, por exemplo)
        * VMs são automaticamente localizadas geograficamente de acordo com a região do projeto
        * VM's são reinicidadas semanalmente
            - Patches
            - Sec Updates
        * Suporta:  microservices, authorization, SQL and NoSQL databases, traffic splitting, logging, search, versioning, security scanning, Memcache, and content delivery networks
        * Suporta personalização da Runtime via:
            - Dockerfile
            - Docker image

+ Diferenças:
    * Standard:
        - Startup: secs
        - Algumas runtimes conseguem escrever no /tmp
        - Libs externas: limitado
        - Acesso à rede: via app engine services
        - Cobrança por classe depois do esgotamento da free tier, tem auto shutdown
    * Flexible
        - Startup: Minutes
        - SSH (necessário habilitar)
        - Escrito em disco local (efêmero)
        - Suporte a libs externas
        - Acesso à rede
        - Cobrança por hora; sem auto shutdown
         
## API management tools

+ 3 tipos de gerenciamento de APIs
    * Cloud Endpoints
        - Sistema de gerenciamento distribuído de APIs
        - Usa um Service proxy (docker container)
        - Customer managed service
    * API Gateway
        -  Serviço gerenciado feito para empacotar funções servless como RESTful API's
    * Apigee API Management
        - Foco em problemas de negócio como rate limiting, quotas e analytics
        - Não ha necessidade dos backends serem executados na GCloud
        - Provê governança, governança e conformidade 
+ Veja mais em: https://cloud.google.com/blog/products/application-modernization/choosing-between-apigee-api-gateway-and-cloud-endpoints


## Cloud Run
+ Plafaforma gerenciada que permite executar *servless containers* via:
    - web requests
    - Pub/sub
+ Serviço servless
+ Construído em cima do Knative (Knative is an Open-Source Enterprise-level
solution to build Serverless and Event Driven Applications)
+ 2 estratégias de publicação
    - Container image
    - Image build via source code
+ HTTP(s) auto enabled
+ Cobrança:
    - Por processamento requests
    - Processamento de startup/shutdown
    - Taxa por milhões de reqs
    - Recursos de CPU e memória
+ Roda linguagens mais atuais, bem como Cobol, Haskell e Perl

## Developing in the cloud

+ Cloud Source Repositories: Git + IAM da Google Cloud
+ Funções servlers iniciadas por algum evento.
    - Java, Node.JS, Python, Ruby
    - Eventos assíncronos: Pub/Sub, Cloud Storage 
    - Eventos síncronos: HTTP
+ IAS - > Terraform
+ Terraform:
    - HCL: Hashicorp Configuration Language
    - Templates armazenados em git, para fins de fluxo de esteira


## Logging and monitoring

Link: https://landing.google.com/sre/books

Texto importante do livro definindo o que é *monitoring*:  
> "Collecting, processing, aggregating, and displaying real-time quantitative data about a system, such as query counts and types, error counts and types,
>  processing times, and server lifetimes."
+ Processos da observabilidade:
    * Monitoring
    * Logging
    * Error reporting
    * Debugging

+ 4 "golden signals" sobre performance de sistemas:
    * Latência (latency) - Tempo para retornar um resultado. Atrelado a: demanda de capacidade, problemas emergentes, medir melhoria de sistema.
        - Page load
        - Num of reqs waiting I/O
        - Query duration
        - Transation duration time
    * Tráfego (traffic) - Num de reqs chegando ao sistema. Atrelado à demanda de sistema atual.
        -  number of HTTP requests per second
        - number of requests for static vs. dynamic content Network I/O
        - number of concurrent sessions number of transactions per second 
        - number of retrievals per second
        - number of write/read ops
        - num of of active connections
    * Saturação (saturation) - Quão próximo da capacidade o sistema está, ou seja, quão "cheio" (saturado) o sistema está.
        - % memory utilization 
        - % thread pool utilization 
        - % cache utilization 
        - % disk utilization 
        - % CPU utilization Disk quota Memory quota
        - Num of available connections
        - Num users on the system
    * Erros (errors) - Eventos que medem as falhas e problemas do sistema.
        - Wrong answers or incorrect content 
        - number of 400/500 HTTP codes
        -  number of failed requests 
        - number of exceptions 
        - number of stack traces Servers that fail liveness checks And 
        - number of dropped connections

+ Tipos de targets:
    * SLI - Service Level Indicator - Um indicador mede um aspecto da confiabilidade do sistema
        - O cálculo é feito: G/V. Sendo G=Num de eventos favoráveis; V= Número de todos os eventos válidos
    * SLO - Combina o SLI com a taxa de confiança desejada.
        - Geralmente numerado como < 100%. Ex.: 99.99% (4 noves);
        - S.M.A.R.T = Specific, Measurable, Achievable, Relevant, Time-bound (refencia tempo. Ex.: 99.9%/ano)
        - Não atingir as SLOs deve ter consequências e ações documentadas, tal qual na SLA
    * SLA - Service Level Agreement - Nível mínimo de serviço acordado com o cliente e planos de ação caso o acordo seja quebrado (SLA não atingido).
        - Geralmente limite de alertas deve ser menor que a SLA

+ Resumindo Observabilidade eficiente:
    1. Capture Signals
    2. Visualize and analyze
    3. Manage Incidents

### Monitoring


+ Os sinais são capturados e tranformados em métricas relevantes. Ex:
    *  Captura-se um sinal, depois transforma em dado estatístico e finalmente produz uma taxa por determinado tempo
        - Ex.: CPU usage signal => CPU usage average  => CPU usage per hour
+ Produto Gcloud: Cloud monitoring

### Logging
+ Produto: Cloud Logging
+ Logs podem ser exportados para o Cloud Storage ou BigQuery
+ Categorias principais de logs (4 categorias chave): 
    *  audit logs
        - Quem fez o que, onde e quando
        - Admin activity
        - Data access
        - System Events - Ações não humanas da GCloud na mudança de configs/recursos;
        - Access Transparency - ações da equipe da google ao acessar seu conteúdo
     * agent logs - FluendD agent
     * network logs 
        - VPC Flow -  monitoring, forensics, real-time security analysis, and expense optimization
        - Firewall rules -  verify, and analyze the effects of your firewall rules.
     * service logs  - Logs criados por devs. Ex.: stdout de um container

### Error reporting

+ Mede os "crashes"/Exceptions
+ Produto: Error reporting
+ Cloud Trace, produto de distributed tracing
+ Cloud Profiler, profiler low-impact da GCloud
+ 
