# Cheatsheet Cloud Storage

```shell
## get Shell Project ID
printenv DEVSHELL_PROJECT_ID

## Copying object from other bucket
gcloud storage cp gs://<SRC_URL> <FILE NAME>

## Copying image to a bucket
gcloud storage cp <FILE> gs://<DST_URL>
gcloud storage cp myfile.png gs://$DEVSHELL_PROJECT_ID/myfile.png


## Modifing access thru ACL
gsutil acl ch -u allUsers:R gs://<URL>
gsutil acl ch -u allUsers:R gs://$DEVSHEL_PROJECT_ID/myfile.png

```


## Cloud SQL Notes

+ Caminho Web Console: Navigation -> SQL -> Create
+ A instância leva uns 5 minutos para ser criada
+ Após a criação é necessário:
    -  adicionar usuário: Users
    - Na aba Connection -> Networking -> ADD, é necessário acrescentar o IP/CIDR range que é permitido acesso

