# Cloud Spanner 

* Serviço escalável/horizontal de RDMBS
* Características
    + Suporta até petabytes
    + Sync repl
    + HA
    + Transações globais
    + Suporta replicação em regiões via Google Global Fiber Network
    + Atomic clocks
* Basicamente a recomendação de uso é para casos em que há necessidade de dividir a carga para ganhar em throughput


# Firestore

* NoSQL db utilizado para sync de dados entre dispositivos mobile, web e IOT em escala global
* Multi-region
* Suporta ACID
* Sem degração de performance
* Strong consistence (transactional)
* Suporta *Scale to zero*
* Recomendado se há necessidade de Consistência transacional (linearizableness) (RYOW)

# Cloud Bigtable

* Serviço gerenciado de NoSQL
* Recomendado caso não seja requerida a consistência transacional (substituída por eventual? no RYOW)
* Petabyte scale
* Mesmo utilizado em:
    + Google Search
    + Google Analytics
    + Gmail
    + Google Maps
* High r/w throughput 
* Baixa latência
* Integração  com Hadoop, Dataflow (Google) e Dataproc (Google)
* Suporta HBase API
* Indexação por key (coluna)
* Uso de column family
* Cada coluna é identifada (column family + qualifier/unique_name)
* Rows+Columns (intersection) contêm múltiplas células com múltiplas versões
* Uso de shards/parititons chamada de tablets (formato sstable)
* Mínimo de nós: 3
* Máximo de nós: 30.000

# Memorystore

* Serviço gerenciado de inMemory Store
* Backed by Redis
* 99.9% SLA availability
* Limite de 300GB e Throughtput de 12 Gb/s
