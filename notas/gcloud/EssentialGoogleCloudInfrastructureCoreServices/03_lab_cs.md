# Lab cloud storage

**Link:** https://www.cloudskillsboost.google/course_sessions/5209909/labs/386466


* O lab iniciou com a criação de um bucket multiregião(us) com controle de acesso por ACL/fine-grained
* Depois fez upload de 3 arquivos para o bucket
* Basicamente em relação à ACL
    ```shell
    # Obtendo ACL do bucket
    gsutil acl get gs://$BUCKET_NAME_1/setup.html  > acl.txt

    # limitando o acesso ao objeto como `Privado`
    gsutil acl set private gs://$BUCKET_NAME_1/setup.html

    # Atualizando acesso AllUsers:R (read-only) para objeto específico
    gsutil acl ch -u AllUsers:R gs://$BUCKET_NAME_1/setup.html

    #

    ```
*  Chaves CSEK para encriptação
    ```shell
    # Criando chave
    python3 -c 'import base64; import os; print(base64.encodebytes(os.urandom(32)))'

    # caso arquivo .boto não exista, cria-se via
    gsutil  config -n 

    # 
    # descomente a chave encryption_key e copie o valor da chave gerada acima
    # salve o arquivo
    vim .boto

    # ao fazer upload, os arquivos já estarão criptografados e precisarão da decryption key para acessá-los
    gsutil cp setup2.html gs://$BUCKET_NAME_1/
    gsutil cp setup3.html gs://$BUCKET_NAME_1/

    # Testando se deu certo
    gsutil cp gs://$BUCKET_NAME_1/setup* ./

    #
    ```
* A partir deste momento, o exercício indicou que a encryption_key deveria ser substituída por uma nova, enquanto o antigo valor seria copiado para a chave decryption_key1
* Assim foi gerada uma nova encryption_key e feito o rewrite dos arquivos com a nova chave
    ```shell
    gsutil rewrite -k gs://$BUCKET_NAME_1/setup2.html
    ```
* Com isso a antiga encryption_key deixou de valer, assim quaisquer outras operações que necessitassem de usar a *decription_key1* (desatualizada), iria incorrer em erro de decriptação.

* O próximo exercício foi relativo ao ciclo de vida:
    ```shell
    # Verificando se o OLM está  ativo
    gsutil lifecycle get gs://$BUCKET_NAME_1
    
    # Criando uma política de delete após 31 dias:
    tee life.son <<EOF 
    {
        "rule":
        [
            {
            "action": {"type": "Delete"},
            "condition": {"age": 31}
            }
        ]
    }
    EOF

    #aplicando política ao bucket
    gsutil lifecycle set life.json gs://$BUCKET_NAME_1

    ```

* O próximo exercício foi de versioning
    ```shell
    # Checando se versioning está habilitado
    gsutil versioning get gs://$BUCKET_NAME_1

    #Habilitando versioning
    gsutil versioning set on gs://$BUCKET_NAME_1

    # Para copiar com versioning, atentar para o switch -v
    # Cada vez que for copiado com este switch, será criada uma nova versão
    gcloud storage cp -v setup.html gs://$BUCKET_NAME_1

    #listando todas as versões:
    gcloud storage ls -a gs://$BUCKET_NAME_1/setup.html

    # baixando versão específica
    gcloud storage cp $VERSION_NAME recovered.txt

    ```

* Sincronizando um diretório e subdirs com o bucket:
    
    ```shell
    gcloud storage cp -r diretorio gs://$BUCKET/diretorio

    #listando
    gcloud storage ls -r gs://$BUCKET/diretorio
    ```

* O último exercício foi para compartilhar bucket entre projetos
* Para isto foi criado um bucket num segundo projeto
    - multiregion
    - fine-grained (ACL)
* Feito upload do arquivo para bucket
* Definido um service account novo com o role *Cloud Storage > Storage Object Viewer* 
* Definida uma nova credencial key no formato json ao clicar na *SA desejada > Keys > Add Key > Create new key*
    - download da key e nomeada como credentials.json
* Depois foi criada uma VM no projeto 1 e acessado o SSH
    - feito o upload do arquivo credentials.json
* Depois foi feita a autenticação com as credenciais fornecidas
    ```shell
    #autorizando
    gcloud auth activate-service-account --key-file credentials.json

    # Listando novo arquivo no bucket do projeto 2
    gcloud storage ls gs://$BUCKET_NAME_2/
    ```
* Ao tentar copiar um arquivo local para o bucket, ocorreu acesso negado, para resolver:
    - Acessado o projeto 2
    - Acessado *Navigation menu > IAM & Admin > IAM > Editar (pencil) SA desejada* 
    - trocado o role da service  account para *Cloud Storage> Storage Object Admin*
    - com isso foi possível executar o seguinte comando com sucesso
    ```shell
    gcloud storage cp credentials.json gs://$BUCKET_NAME_2/
    ```






