# Cloud SQL

* Serviço gerenciado de RDMBS
* "Cost effective" em detrimento da necessidade de horizontal scaling ou disponibilidade global
* Clientes
    + Cloud Shell
    + App Engine
    + Google Workspace Scripts
* Outras características
    + Até 64 TB de capacidade
    + 624 GB de RAM
    + 96 cpus
* Bancos:
    + MySQL (5.6, 5.7, or 8.0 )
    + PostgreSQL (9.6, 10, 11, 12, 13, or 14) 
    + Web, Express, Standard or Enterprise SQL Server 2017 or 2019 editions.
* HA Config:
    + Regional instance
    + Primary + Standby instance
        - Todas as escritas são replicadas da Primary para Standby (2PC)
        - Caso a primary caia, a Standby torna-se-á primária
    + Suporta "scale up", mas requer restart ou estratégia de *scale out*/(horizontal scaling) usando *read replicas*
+ Caso seja necessário conexão a outro projeto ou região, ou tentando conectar-se fora da Google Cloud:
    + Cloud SQL Auth Proxy -> conexão encriptada, autenticada e com rotação de chaves (manual/automática)
    + Por meio de autorização de IP Público específico