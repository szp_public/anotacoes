# Resource Management

* Existem cotas máximas de uso por projeto, mas um aumento pode ser solicitado
* RM permite organizar hierarquicamente os recursos

> **Para lembrar**
> * *Policies* -> conjunto de Roles e Membros atribuídos a recursos

* Os custos são acumulados pelo o que os recursos usam  
    + acumulados no projeto
* Cada projeto é associado a uma única billing account
* Uma Org contêm todas as billing accounts
* Hierarquia dos recursos   
    + Global 
        - Images
        - Snapshots
        - Networks
    + Regional 
        - External IP
    + Zonal
        - Instance Disks
* Basicamente as  quotas são usadas para prevenir abuso. Não foi dito isso no curso, mas entendi desta forma

> **Nota Importante:**
> Mesmo que não haja consumido toda a quota, se algum recurso estiver esgotado na região, não será possível alocá-lo.
> Ex.: supondo que haja 50% de discos consumidos até chegar a quota, mas seja necessário alocar mais 25%, não é garantido que 
> será possível completar a operação, pois se a região estier sem SSD's disponíveis, não será possível alocar mais, a despeito >
> da quota 25% restante

* **Labels**: São formas de identificar melhor os recursos, são do tipo key value pair (par chave-valor)
    + Limite de 64 labels por resource
    + Ajuda a identificar  melhor os recursos nos relatórios de custos
    + Recomendação: Criar labels baseado no time/equipe ou no centro de custo. Ex.:
        - team:marketing; team:research
        - component:redis; component:frontend
        - environment:production;  environment:dev
        - owner:gaurav; owner:opm
        - state:inuse; state:readyfordeletion

* Labels não são tags 
    - Labels  são utilizadas para organizar recursos para que possam ser propagadas para o billing
    - Tags são aplicadas somente nas instâncias e são utilizadas, principalmente, para networking. Ex.: aplicar regras de firewall
* É possivel definir um budget/orçamento: 
    + rastrear e controlar custos
    + Alertas de consumo para os billing admins sobre o consumo em relação aos thresholds
        - Cloud Pub/Sub
        - Email
* É possível, na BigQuery, fazer uma query que traz o custo por label. Ex:
    ```SQL
        --BIGQUERY
        SELECT
            TO_JSON_STRING(labels) as labels,
            sum(cost) as cost
        FROM `project.dataset.table`
        GROUP BY labels;
    ```
* É possível exportar os dados de billing para arquivo ou BigQuery (dataset)