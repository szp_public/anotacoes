# StackDriver

* Renomeado para Pacote de operações do **Google Cloud Operations Suite** (GCOS ou COS)
* Descoberta de recursos com forte integração com: AWS e GCP
* Compreende:
  + Cloud Monitoring
  + Logging: 
    - Cloud Logging  
    - Error Reporting
  + APM
    - Cloud Trace
    - Cloud Profiler

# Cloud Monitoring

* Configurado automaticamente após deploy dos recursos
* Monitoráveis:
  + Plataformas
  + System (SO)
  + Applications
* Estratégia por ingestão:
  + Métricas
  + Eventos
  + Metadados
* Metric scope: entidade raiz de configurações na Cloud Monitoring
  + Cada uma pode ter entre 1 e 100 projetos monitorados
  + Quantas metric scopes necessárias, mas GCP Projects e AWS Accounts não podem ser montirados por mais de uma metric scope
  + Primeiro projeto configurado de uma metric scope é chamado de *hosting project*
  + Todos os usuários do GCOS com acesso a uma metric scope tem acesso a todos os dados, por padrão
    - Recomendável criar metric scopes separadas caso haja roles diferentes por projeto, evitando acesso indesejado
* Alertas podem ser configurados
  + Canais
    - SMS 
    - Email
    - Outros canais (API? Pagerduty?)
  + Configuração por thesholds/condições
  + Melhores práticas
    - Criar alertas baseados em sintomas, não na causa
    - Assegurar-se que vários canais de notificação estão configurados
    - Descrever, nos alertas, o que precisa ser feito (ações) ou recursos que precisam ser examinados
    - Evite excesso de alertas
    - Evite dados irrelevantes (noise) que possam complicar a análise
* É possível configurar métricas personalizadas


