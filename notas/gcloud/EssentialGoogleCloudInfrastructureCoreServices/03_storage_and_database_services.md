# Storage and database services

* Tipos:
    + Cloud Storage -> Object
    + Files -> File Store (NAS/NFS)
    + Cloud SQL -> RDMBS de bom custo/benefício
    + Cloud Spanner -> Distributed RDMBS
    + Firestore -> Mobile DB
    + Cloud Bigtable -> Heavy read/write data processing
    + BigQuery -> Data Warehouse -> OLAP + interactive query
    + Memory Store -> inMemory cache DB
* Recomendações:
    + BigQuery 
        - Dados que não mudam constantemente
        - Dados tabulares
        - Adhoc Queries
    + Bigtable
        - Wide column db
        - baixa latência 
        - alto número de r/w
        - Dados muito grandes para cabem inMemory

## Cloud storage

* Object storage
    + Object = Content(file) + metadata
    + Embora Objects pareçam com arquivos, também podem ser referência a outros Objetos, sendo possível distribuição hierárquica tal qual feita divisão me pastas e arquivos

* Quatro (4) classes:
    + Standand
        - Hot data
        - Mais caro
        - Recomendado para dados temporários e leituras mais constantes
    + Nearline
        - Duração mínima 30 dias
        - Custo por obtenção: US$ 0.01/GB
        - Recomendável para leituras 1x a cada 30 dias
    + Coldline
        - Duração mínima 90 dias
        - Custo por obtenção: US$ 0.02/Gb
        - Recomendável para leituras 1x a cada 90 dias
    + Archive
        - Duração mínima: 365 dias
        - Custo por obtenção: US$ 0.05/Gb
        - Não possui SLA de disponibilidade
        - Recomendável para leituras 1x ao ano
* Quando um object é adicionado a um bucket, será utilizada a classe associada ao bucket
* Regional bucket pode ser trocado para multiregion, mas o inverso não é possível
* Objetos podem ser movidos entre buckets
* o serviço OLM ( Object Lifecycle Management) pode gerenciar as classes de objetos individualmente
* IAM pode ser usado para controlar acesso aos buckets. Ex: (list, create, view name)
* ACLs podem ser utilizada para um acesso fine-grained
    + Limite de 100 por bucket
* É possível usar uma URL criptograficamente assinada para dar acesso expirável a um bucket ou object
    + Existe a funcionalidade, de signed policy, que limita os tipos de arquivos que podem ser publicados no bucket
    + Essa URL é assinada mediante private key associada a uma service account específica, sendo qué possível definir política de expiração 
    + Quem receber esta URL pode executar as mesmas operações que a SA associada à key
    + Ex.
    ```shell
    # gerando a URL com acesso de PUT
    gcloud storage sign-url gs://BUCKET_NAME/OBJECT_NAME --private-key-file=KEY_FILE --http-verb=PUT --duration=1h --headers=Content-Type=CONTENT_TYPE
    # Exemplo de resposta
    ---
    #expiration: '2023-07-14 23:35:47'
    #http_verb: PUT
    #resource: gs://example-bucket/cat.png
    #signed_url: https://storage.googleapis.com/example-bucket/cat.png?
    #x-goog-signature=2f670a686102963e0574f3c1a3b4d29ee4aa406c1528d42d2
    #30195d17fef73834b254314de7d7990afd48538a84b66f20010e7ecd90a900490e
    #6119b7e56a912f71c8d64285c40e86f31b8fec51cf8c7a61ded81de3cedac9c1ca
    #b92474b7371740fdac20b2d8d092b15396f79443bbde954a4174ed11aef6c2cf5f
    #a4d72a84ff60fd6003ed0a505b0e40b6207ddbaec2a15778f715c3ec7537a1b14f
    #b6661b2abaa5736f1670a412ca7e2555c830591f0595c01ff95af7f2206abe2e27
    #41948c16d4bd4c7cbb25f41277ece59236c06e00ca6c63ae2eb3efc22c216bb24c
    #e1b8b3801d07fd3a7ed3f2df3db6e59c6fc3cc76a002335dd936efd0237cf584e3
    #6&x-goog-algorithm=GOOG4-RSA-SHA256&x-goog-credential=example%40ex
    #ample-project.iam.gserviceaccount.com%2F20230714%2Fus%2Fstorage%2F
    #goog4_request&x-goog-date=20230714T223547Z&x-goog-expires=3600&x-g
    #oog-signedheaders=Content-Type%3Bhost
    ```

### Outras caraterísticas
+ uso de CSEK (customer managed encryption keys) em vez do CMEK(gloogle managed enc key)
+ OLM 
    - Automaticamente deletar, arquivar ou mudar de classe;
    - Aplicáveis a todos os objetos do bucket mediante critérios das regras
    - Alterações no OLM levam até 24h para serem aplicadas
+ Versionamento de objetos 
    - Cobrança por versão, pois cada versão é um arquivo
    - quanto o versionamento é desabilitado, as versões são mantidas, porém novas alterações não serão acumuladas
+ Sync de diretório com bucket (FUSE???)
+ Pub/sub para notificações de alteração de objetos
+ Objetos são imutáveis durante o tempo de vida

### Importação de arquviso  

* Para larga importação de arquivos é possível utilizar os serviços:
    + Transfer Appliance
        - Basicamente o Google te manda um equipamento appliance em que o cliente copia os dados para este e envia de volta
    + Storage Transfer service
        - Transferência com outros file storages (google, aws, azure, on-premises/http(s) )
    + Offline Media Import
        - Via provedor parceiro via discos, flash drives e outros dispositivos de armazenamento

### Consistência

* Inexistência de 404 para read-after-write/read-after-metadata-update
+ 404 imediato após remoção


## Filestore

* Serviço gerenciado de arquivos para sistemas que precisam de um sistema de arquivos compartilhado (eg NFS3/NAS)
* Baixa latência de operações
* Espaço "elástico", de acordo com a necessidade
* 