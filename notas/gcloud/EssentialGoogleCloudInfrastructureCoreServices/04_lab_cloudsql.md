# Lab Cloud SQL

* Para este lab, já haviam 2 instâncias pré configuradas
    + wordpress-private-ip
    + wordpress-proxy
        - Nesta máquina foi instalado a aplicação cloud_sql_proxy
        ```shell
        wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy && chmod +x cloud_sql_proxy
        ./cloud_sql_proxy -instances=$SQL_CONNECTION=tcp:3306 &
        ```
* Durante o lab, fora criada uma instância do Cloud SQL via `Navigation Menu > SQL > Create Instance > Choose MySQL`
    + Machine type, dedicated CPU
    + limitação de 250MB/s de network throughput por vCPU
    + Selecionado SSD de 10GB, sendo importante ressaltar que capacidades maiores tem melhores taxas de IOPS
    + Foi selecionada a opção de Private IP
        - Network default
        - Setup Connection > Enable API > Use an automatically allocated IP range > Create connection     
* Conexão via Proxy
    + Acessado o IP externo do worpress-proxy
        - Utilizado o 127.0.0.1 como Dabase Host, tendo em vista que é um proxy
* Conexão via Private IP
    + Acessado o IP externo do wordpress-private-ip
    + Configurado o Private IP como Database Host


> **Nota:**
> O interessante é que o Cloud Auth Proxy precisa de uma VM instance para servir como proxy, não sendo um serviço nativo. Desta forma é preciso gerenciar/observar a operabilidade da instância que servirá de proxy
> 
> https://cloud.google.com/sql/docs/mysql/sql-proxy?hl=pt-br#install