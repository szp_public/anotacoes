# Essential Google Cloud Infrastructure: Core Services

## IAM 

* Sistema de políticas de permissões de acesso da Google Cloud
* Organização hierárquica: Org Node -> Folders -> Projects -> Resources
* Org Node Roles:
    + Org Admin
    + Project creator
* Intimamente ligado ao G Suite (Workspace) ou Cloud Identity
    + Quando um projeto é criado, uma org resource é automaticamente criada
* Cloud identity/GSuite Super admin
    + role chave para setup e controle de ciclo de vida
    + responsável por
        - associar org admin role para usuários
        - ponto de contato no caso de recuperação de falhas
* Org admin
    + Definir políticas IAM 
    + Definir estrutura hierárquica de recursos
    + Delegar responsabilidade sobre componentes críticos como: Networking, Billing e Resource
    + Não possui permissão para executar ações  (ex.: criar folder)
        - Será preciso associar roles adicionais
* Folders:
    + Análogo a uma sub-org ou uma OU (org unit)
* Resource manager roles:
    + Org -> Admin, Viewer
    + Folder -> Admin, Creator, Viewer
    + Project -> Creator, Deleter
    >  Obs.: As permissões são herdadas
* Tipos de Roles:
    + Basic 
        - Owner
        - Editor
        - Viewer
        - Billing Admin
        > Permissões concênctricas -> Owner{Editor{Admin}}
    + Predefined
        - se aplicam ao nível de serviço (Ex.: role de Instance Admin no projeto A)
        - Cada serviço tem um conjunto diferente de predefined roles
        > Ex: GCE possui os roles Compute Admin, Network Admin, Storage Admin
    + Custom 
        - Roles para definição granular de permissões. Ex.: Permitir Iniciar/Parar instâncias, mas não reconfigurá-las.
        - Caso haja alguma alteração no sistema de permissões da GCP, será necessário reconfigurar estes roles

* Members, tipos:
    + Google Account
        - Qualquer pessoa que interage com a Google Cloud
        - Utiliza e-mail para associação.
        - Qualquer domínio de e-mail
    + Service Account
        + conta de aplicação
    + Google Groups
        - Conjunto de Google e Service accounts
        - Cada grupo tem um e-mail associado
    + Google Workspace domains
        - Grupo virtual de todas as contas em uma workspace da org
        - Representa um domínio (ex.: example.com)
        - Quando um usuário é adicionado é criado um email virtual: usuario1@example.com
    + Google Cloud Identity domain
        - Usuários gerenciados pelo serviço Cloud Identity
        - Não ganham Drive, Gmail e outros serviços do google
        - IAM não pode ser usado para criar ou gerenciar  usuários ou grupos
            - Feito pelo Google Workspace ou Cloud Identity
* **IAM Policies**
    + é a lista de *bindings*( lista de membros de um role)
    ```json
        { 
            "bindings": [
                {"members":["user": "user1@example.com"], "role": "roles/resourcemanager.organizationAdmin"},
                {"members":["user": "user1@example.com","user2@example.com"], "role": "roles/resourcemanager.projectCreator"},
            ],
            "etag": "....",
            "version": 1
        }
    ```
    + Role é uma lista nomeada de permissões definidas pela IAM
    + As políticas mais restritivas são sobrescritas pelas menos restritivas do parent
        - Ex.: Folder AA (role Editor) sobrescreve permissões de Projeto AA01 (viewer role)
    + Permite criação de *deny rules*
        - São sempre checadas antes das políticas de allow
    + IAM Conditions podem ser aplicadas
        - Ex.: Possível definir permissões de *grant* somente quando um conjunto de condições for satisfeita
            - Permitir acesso temporário a um ambiente produtivo somente quando houveer um problema
            - limitar acesso somente por localidade (corporate office)
            - são especificadas nas role bindings
* **Org Policies**
    + Restrições a nível de organization nodes
    + Podem ser aplicadas em Orgs, Folders e projects

* Gcloud possui Dir Sync (LDAP ou Ms AD)
    + One-way
* SSO é possível via SAML2. Com outros provedores é necessário usar outro serviço (ex. Okta, ADFS, Ping) 

* **Service Account**
    + São contas que pertencem a uma aplicação
    + Identificáveis por e-mail, ex: 12345678909-compute@project.gserviceaccount.com
    + Tipos:
        - user created
        - built-in
        - Google API
            - Todos os projetos vem com SA embutido no formato: projectnumber + @cloudservices.gserviceaccount.com
                - Criadas para executar  GCloud Services
                - Editor Role
    + CE Default SA
        - formato: PROJECT_NUMBER-compute@developer.gserviceaccount.com
        - embutido na criação/início da instância
    + É possível definir escopos de acesso na criação da instância na seção *Intentity and API Access*
        - Seleciona-se a SA desejada
        - Escolhe-se a opção *Set access for each API* 
        - Esse é o modelo legado, o corrente é via IAM Role
    
* **Service account authentication** 
    + Tipos
        - Google managed sa
            - Chaves públicas e privadas são armazenadas pelo google
            - São rotacionadas regularmente
            - private keys não são acessíveis
            - cada chave só pode ser utilizada para assinatura por 2 semanas
            - User managed key pairs:
                - Google fica com a chave pública
                - Gerenciamento por conta do usuário
                - Até 10 keys por serviço
                - Google não armazena as private keys, responsabilidade dos admins fazerem isso
        - user managed sa
                - para casos em que o uso da SA seja externo
                - Gerenciamento por conta do usuário
                - Até 10 keys por serviço
                - Google não armazena as private keys, responsabilidade dos admins fazerem isso
                - Gerenciadas via: Cloud IAM, gcloud ou console

### Best practices 

* Projetos para agrupar recursos do mesmo âmbito de confiança
* Verifique as políticas de liberação para cada recurso garantindo as heranças
* Devido à herança, use o princípio do menor acesso ao definir *roles*
* Audite as políticas(policies) e membresia dos grupos via Cloud Audit Logs
* Definir roles para grupos invés de indivíduos
* Gerencie a propriedade(ownership) e políticas dos grupo usadas na Cloud IAM
* Utilize múltiplos grupos para melhor controle. Ex.:
    + Groups = [Network_Admin_Group, Group_RO_CloudStorage, Group_RW_CloudStorage]
* Cuidado ao associar o role *serviceAccountUser*, pois permite acesso a todos os recursos que uma *service account* tem
* Ao criar uma SA o nome dela deve refletir claramente o propósito (mnemônica) utilizando uma convenção de nomenclatura
* Estabeleça uma política de rotação das chaves e as audite com o método `serviceAccounts.keys.list`
* Utilize o Cloud IAP (Identity Aware Proxy), garantindo uma camada central de autorização via HTTPS
    + O acesso é garantido/autorizado de acordo com as políticas de IAM para cada usuário/grupo

    