# Lab: Examining Billing Data with BigQuery

* Neste lab um CSV exemplo fora disponibilizado pois não era possível ter acesso como Org Admin

> **Nota**
>
> É possível exportar billing data para BigQuery da seguinte forma: https://cloud.google.com/billing/docs/how-to/export-data-bigquery

* Foi acessado o BigQuery e importado o relatório para um dataset via: `Navigation menu > BigQuery > Done; View Actions (3 pontinhos) > Create Dataset`
  - Ex.: Maximum table age foi ajustado para 1 (days)
* Foi criada uma tabela através do dataset criado via: `View Actions (3 pontinhos) do dataset > Open > Create Table`
    + Create table from: Google Cloud Storage
    + Select file from GCS bucket cloud-training/archinfra/export-billing-example.csv
    + File format CSV
    + Destination: 
        + Table name: sampleinfotable 
        + Table type: Native table 
        + Schema: Autodetect 
        + Advanced 
            - header rows to skip: 1

* Após isso, o lab utilizou outro dataset público: `cloud-training-prod-bucket.arch_infra.billing_data`
* A partir daí foram feitas as seguintes queries:
    ```SQL
    --Dados
        SELECT
            product,
            resource_type,
            start_time,
            end_time,
            cost,
            project_id,
            project_name,
            project_labels_key,
            currency,
            currency_conversion_rate,
            usage_amount,
            usage_unit
        FROM
        `cloud-training-prod-bucket.arch_infra.billing_data`

   -- To find the latest 100 records where there were charges (cost > 0), fo
    SELECT
        product,
        resource_type,
        start_time,
        end_time,
        cost,
        project_id,
        project_name,
        project_labels_key,
        currency,
        currency_conversion_rate,
        usage_amount,
        usage_unit
    FROM
    `cloud-training-prod-bucket.arch_infra.billing_data`
    WHERE
    Cost > 0
    ORDER BY end_time DESC
    LIMIT 10

    --To find all charges that were more than 3 dollars
    SELECT
        product,
        resource_type,
        start_time,
        end_time,
        cost,
        project_id,
        project_name,
        project_labels_key,
        currency,
        currency_conversion_rate,
        usage_amount,
        usage_unit
    FROM
    `cloud-training-prod-bucket.arch_infra.billing_data`
    WHERE
        cost > 3

    --To find the product with the most records in the billing data,
    SELECT
            product,
            COUNT(*) AS billing_records
    FROM
            `cloud-training-prod-bucket.arch_infra.billing_data`
    GROUP BY
            product
    ORDER BY billing_records DESC
    
    -- To find the most frequently used product costing more than 1 dollar,
    SELECT
        product,
        COUNT(*) AS billing_records
    FROM
        `cloud-training-prod-bucket.arch_infra.billing_data`
    WHERE
        cost > 1
    GROUP BY
        product
    ORDER BY
        billing_records DESC

    -- To find the most commonly charged unit of measure,
    SELECT
        usage_unit,
        COUNT(*) AS billing_records
    FROM
        `cloud-training-prod-bucket.arch_infra.billing_data`
    WHERE cost > 0
    GROUP BY
        usage_unit
    ORDER BY
        billing_records DESC

    -- To find the product with the highest aggregate cost,
    SELECT
        product,
        ROUND(SUM(cost),2) AS total_cost
    FROM
        `cloud-training-prod-bucket.arch_infra.billing_data`
    GROUP BY
        product
    ORDER BY
        total_cost DESC

    ```
