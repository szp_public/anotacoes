# Lab IAM


* O lab propôs um setup inicial com 2 usuários
    + usuário 1 - project owner
    + usuário 2 - project viewer
* Mesmo o usuário 2 sendo Viewer, ele pôde visualizar os roles associados ao projeto via `IAM & Admin > IAM`
* Foi removido o acesso do *Usuario 2*, garantido acesso de `Storage Object Viewer`
    + Desta forma o usuário não pode acessar via Cloud Console
    + Porém o usuário 2 tinha acesso via Cloud shell `gcloud storage ls gs://$BUCKET_NAME`
* No próximo exercício, fora criada uma Service Account chamada *read-bucket-objects* com o role *Storage Object Viewer*
* Em seguida foi adicionado um domínio inteiro ao SA criado anteriormente via `Service Accounts -> read-bucket-objects -> Manage permissions -> Grant Access`
    + role: *Service Accounts > Service Account User*
    + New Principals: *altostrat.com*
    + Dessa forma, todos os usuário associados a este domínio tem acesso à SA criada
* Via `IAM & Admin > IAM > Grant access` foi dado acesso de:
    + role: *Compute Engine > Compute Instance Admin (v1)*
    +  New Principals: *altostrat.com* 
    + Com isso os usuários do domínio *altostrat.com* tem acesso ao SSH da VM e podem executar algumas operações administrativas
* Depois, foi criada uma VM associando a service account *read-bucket-objects* 
* Ao tentar, via SSH, listar compute instances (`gcloud compute instances list`) houve um erro.
    + Isso ocorreu porque a SA não possuía a permissão de `compute.zones.list` para este projeto
* Ao tentar, via SSH, copiar arquivos do bucket, houve sucesso (`gcloud storage cp gs://$bucket_name/sample.txt .`)
    + Porém houve erro ao tentar copiar para o bucket, pois a permissão *storage.objects.create* não havia sido associada à SA associada à VM
