IAM Roles

Básicos: mais geral da google cloud e serviços.

Prédefinidos: mais granulares em relação aos serviços do projeto

Boa prática: Em ambiente de de produção, conceder acessos pré-definidos, pois os papéis básicos tem milhares de permissões atreladas

É possível que um usuário tenha mais de um role associado a ele.


