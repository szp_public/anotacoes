# Getting Real About Distributed System Reliability


**referência**: https://blog.empathybox.com/post/19574936361/getting-real-about-distributed-system-reliability

*Author:* Jay Kreps (Desenvolvedor do Voldemort (NOSQL))


## Resumo/Notas

O autor menciona o hype sobre os famigerados **distributed data systems** (DDS), centralizando os serviços em provedores de nuvem de larga escala. Ele enfaticamente menciona que escalabilidade incremental e horizontal requer completo *redesign* dos softwares.

O autor cita as seguintes propriedades de um DDS:
1. Capacidade de operar com os *downtimes* planejados ou janelas de manutenção de forma que os sistemas tradicionais não conseguem alcançar;
2. Os sistemas DDS são projetados para lidar com falhas de hardware(máquinas) de forma diferente do tradicional.

O autor alega que qualquer sistema tal qual Cassandra ou Hadoop pode não tolerar falhas de máquinas. Isso se deve à necessidade particionar os dados/estado e replicá-lo entre os vários nós da ferramenta escolhida. Assim, ele determina que para *N* replicações em que sejam toleradas *N-1* falhas, há probabilidade *P<sup>N</sub>* de perder as informações, assim um índice de confiança R desejado precisa ser *P<sup>N</sup> < R*.

O autor também comenta que as falhas de máquina são correlacionadas, sendo um erro assumir que são independentes. Assim, que for percebido um erro em uma máquina, o mesmo bug será observado em todas as outras máquinas do cluster. Por exemplo, uma configuração mal feita ou uma aplicação malformada irá gerar falhas independente do número de replicas que existam. 

O autor também comenta que a confiança de um sistema depende do quão isento de *bugs* ele é. Além disto, um sistema como Zookeeper tende a ter muito menos profissionais no mercado que um Oracle ou MySQL, pois está há muito menos tempo no mercado. Então, a maturidade é a chave para o sucesso de um DDS.

Em relação à detecção de falhas, torna-se muito mais difícil pois é difícil simular/validar os erros que possam ocorrer em um nó que esteja degradado.

Segundo o autor, a configuração e setup é extremamente complexa, exigindo muito conhecimento e diligência da equipe de operações sobre a ferramenta implantada.

O autor cita um exemplo de *bug* do Kafka, que ao receber uma requisição corrompida, interpretava como erro de log corrompido. Os nós que recebiam esta requisição acabavam sendo desligados automaticamente, assim, como a requisição estava sendo enviada por um cliente com *bug*, todos os nós acabaram ficando fora de operação.


O autor comenta que só confia em um serviço que promete, por exemplo, 1 ano sem *downtime* quando há evidências que já executou por 1 ano sem nenhum *downtime*.

O restante do artigo o autor sustenta sua opinião sobre a necessidade de haver evidências empíricas que comprovem a confiança (reliability) dos DDS.


## Outros aspectos importantes

- Sempre questionar os vendedores sobre os [MTBF](https://en.wikipedia.org/wiki/Mean_time_between_failures) (Mean Time Between Failures) e os [MTTR](https://en.wikipedia.org/wiki/Mean_time_to_recovery) (Mean time to recovery) com evidências reais da distribuição dessas métricas. Dessa forma se evita a analisar somente os casos de sucesso;
- Também é importante questionar como são obtidas essas métricas, se são por reclamações dos clientes ou por sistema automatizado de verificação;
- Foco em contratar os serviços, deixando a operação para o fabricante. Cabe a nós trabalhar na nossa especialidade;
