# Chapter 01

## Types of Cloud services

### Compute resources

+ Virtual Machines
+ Managed Kubernetes
+ Servless computing
    - App engine - para aplicações que precisam rodar por período extendido, ex: webapp, webservice, etc.
    - Cloud run - Para executar containers sem utilizar toda a estrutura do managed kubernetes
    - Cloud functions - executar aplicações em resposta a um evento, como por exemplo: mensagem em uma fila ou um upload de arquivos, webhooks, firebase, etc. Ou seja, existe uma trigger iniciando as funções


### Storage

+ Object storage - Armazenamento de objetos ( dados + metadados) acessíveis via URL. É um serviço de armazenamento que não leva em conta espaço disponível para armazenamento ou necessidade de criação de VM's para gerenciá-los. O acesso aos dados é mais lento que no modelo Block storage, porém é possível armazená-los em um block storage para superar essa limitação.
+ File storage - File system storage baseado no NFS, em que um volume pode ser montado por várias VM's ou outros serviços gerenciados.
+ Block storage - Análogo aos discos de um sistema operacional, que são acoplados a uma VM. Podem ser do tipo ephemeral ou persistents. Sendo que os ephemeral tem seus dados removidos quando a VM é desligada, diferente do modelo persistent
+ Caches 
    - Armazenamento de dados em memória
    - 4x mais rápido que o SSD, para leitura sequencial
    - 80x mais rápido que HDD, para leitura sequencial

### Networking

+ VPC - Forma como a rede interna é definida. Obs: Lembra muito o conceito de DMZ
+ Rede externa - Forma como os serviços são acessíveis por recursos externos à VPC, os IPs podem ser: estáticos ou dinâmicos
+ Firewall é usado para limitar acesso a recursos (inbound/outbound) dentro de uma VPC. Ex.: Somente um servidor de aplicação específico pode acessar o database;
+ Peering - estratégia de acesso a recursos de sua VPC, como:
    - VPN
    - Interconnects
    - Shared VPC
    - Direct/Carrier peering

### Specialized Services

Serviços adicionais fornecidos pela nuvem para auxiliar as aplicações, como:
+ Funções
+ API Gateways
+ Recursos de IA
+ Serviços gerenciados
+ Message Queues


## Resumindo: Cloud Computing vs Data Center Computing

- Rent instead of Own Resources - neste modelo é possível "alugar" recursos quando houver um período de maior carga que o normal. Em um DC tradicional esse modelo não é possível, limitando elasticidade em uma sazonalidade
- Pay as you go - A cobrança em nuvem é pelo período de utilização (limite mínimo de 1min);
- Elastic Resource allocation - Em cloud é possível alocar vários recursos em minutos, por um simples comando ou um clique de botão, enquanto em um DC tradicional é necessário um árduo e longo trabalho para que haja elasticidade, o que pode incorrer em "perda de timing"
- Specialized Services - Em cloud é possível contratar serviços especializados, como IA, em poucos minutos. Usar as API's e outros recursos aumentando a eficiencia dos processos internos.





