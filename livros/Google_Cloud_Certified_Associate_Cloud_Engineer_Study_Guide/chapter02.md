# Google Cloud Computing Services

## Computing Resources

+ IaaS - Modalidade em que é possível instanciar VM's, bem como S.O., packages, backup de forma operacional/manual. Ex.: Compute Engine
+ PaaS - Modalidade em que é fornecida um ambiente (Runtime environment)  para executar aplicações sem se preocoupar com os servidores, redes e storages deste ambiente. Ex.: Cloud Functions, App Engine.

### Compute Engine

+ Serviço que permite criação de VM's na GCloud
+ É possível acoplar storages persistentes e utilizar outros serviços, como Cloud Storage
+ VM's rodam em um serviço de baixo nível (low-level) chamado hypervisor
+ O Hypervisor utilizado na GCloud é baseado no KVM (Kernel Virtual Machine)
+ É possível personalizar VM's:
    - SO
    - Tamanho do storage
    - GPU
    - se a VM é preemptiva
        * 80% menos custo (em média)
        * Pode ser finalizada a qualquer momento pela GCloud
        * Será desligada/finalizada quando tiver em execução por pelo menos 24h
    - Spot instances
        * VM preemptivas que não estão sujeitas ao limite de 24h, tampouco tempo máximo de runtime.


### Kubernetes Engine 

+ Serviço de orquestração de contêineres
+ Container Manager é o serviço low-level  utilizado para executar os contêineres
+ Anthos é o serviço utilizado para gerenciar múltiplos clusteres Kubernetes (estilo federação)
    - Multiclusters
    - Híbrido
    - Conectados via VPN, Interconnect dedicada, etc...
    - Gerenciamento centralizado de clusters K8s
    - Rollback por meio de Git
    - Service Mesh authorization and routing

### App Engine

+ Serviço PaaS para executar aplicações no modelo Servless
+ Toda a rede e computação subjacente é gerenciada por esse serviço. Não há necessidade de preocupar-se com esses detalhes
    - Segurança (hardening) de rede
    - Configurações de VM's
+ Possui 2 tipos de ambientes:
    - Standard - A aplicação é executa em uma sandbox prefixada e não personalizável.  Outras aplicações podem executar no mesmo servidor. Utilizada caso a aplicação não precise de nenhum pacote do SO ou qualquer outro componente compilado junto com a aplicação
    - Flexível - Em que executado de forma conteinerizada, sendo personalizável, por exemplo, sendo possível trabalhar com processos em background e escrever em disco

### Cloud Run

Serviço para execução de contêineres. A diferença entre este e App Engine Std é que ele não está restrito a uma lista de linguagens pré-determinadas. Outras características:
+ Um serviço pode ter múltiplas regiões
+ Um serviço pode ter múltiplas revisões
+ Há autoscaling baseadas na carga
+ As diferenças entre o GAE e o RUN estão descritas nessa doc: https://cloud.google.com/appengine/migration-center/run/compare-gae-with-run

### Cloud Functions

+ Serviço de computação leve voltado para processamento "event-driven"
+ O código executado deve ter vida curta, para um único propósito
+ É um produto Servless


## Storage components of Google Cloud - Storage Resources

### Cloud Storage 

+ Object storage system
+ Organizado em buckets (análogos a diretórios)
+ Objetos são endereçados unicamente via URL
+ Privilégios de acesso são concedidos via IAM roles
+ Armazenamento por região. Modelos:
    - Single Region
    - Dual-region
    - Multi-region w
+ Possível definir políticas de retenção, expiração e movimentação entre classes de storage baseado na frequência de acesso


### Persistent disk
+ Serviços de storages acoplados/montados a uma VM
+  Tipos:
    - SSD
    - HDD
+ Até 64TB de tamanho
+ Múltiplos PDs podem ser acoplados a uma VM

### Cloud Storage for Firebase

Serviço de armazenamento de objetos para aplicações mobile. 
+ API:
    - Transmissão segura
    - Mecanismos para lidar com instabilidade de rede (network quality)

### Cloud Filestore
+ Sistema de armazenamento de arquivos que implementa o protocolo NFS.
+ Provê alto número de IOPS
    - No corrente momento (31/08/2023) um serviço BASIC_SSD fornece:
        * 60,000/25,000 r/w IOPS
        * 1,200/350 r/w MiB/s (mebibytes/s)

## Databases

### Cloud SQL
+ Serviço gerenciado de banco de dados sem necessidade de utilização de VMs (Compute Engine) para isso
    + Mysql
    + PostgreSQL
    + SQL Server
+ Automatic failover
+ Não é preciso preocupar-se com tarefas de DBA (backup, patch, etc...)


### Cloud Bigtable
+ Desenhado para aplicações de escala Petabyte
+ Ideal para Bilhões de linhas e milhares de colunas
+ R/W com baixa latência, desenhado para milhões de operações por segundo
+ Integra com:
    - Cloud Storage 
    - Cloud Pub/sub
    - Cloud Dataflow
    - Cloud Dataproc

### Cloud Spanner
+ Banco relacional da GCloud
+ 99.999% de disponibilidade (SLA)
+ Possibilidade de escala horizontal tal qual NoSQL

### Cloud Firestore (antigo Datastore)
+ NoSQL Document DB
+ Acessível via REST API
+ Escalabilidade automática, baseado no load
+ Utiliza da estratégia de *shards* ou *partitions*
+ Suporta:
    - SQL-like queries
    - Indexes
    - Transactions

### Cloud MemoryStore
+ In-memory cache
+ Suporta:
    - Redis
    - memcached

---

## Networking Services

### Virtual Private Cloud (VPC)

+ É uma "emulação"/"simulação" de uma rede privada de uma organização, executado em ambiente Cloud
+ É importante ressaltar que todos os clientes de Clouds utilizam a mesma estrutura de hardware, porém separados logicamente
+ Para executar uma VPC, não é preciso que haja conectividade com a internet
+ A comunicação entre uma VPC e  uma rede privada on premises é feita através do protocolo IPSec
+ É possível separar o billing de cada conta VPC


### Cloud Load Balancing 

+ É o serviço de distribuição de caga da nuvem
+ É possível configurar escalabilidade automática baseada em degradação/carga de servidores, roteamento, etc
+ Balanceia: HTTP, HTTPS, TCP/SSL e UDP


### Cloud Armor
É o WAF da Google Cloud. É possível:
+ Proteger contra DDoS
+ Restringir acesso por IP
+ Definir regras em camada 3 (rede) e camada 7 (aplicação)
+ Restringir acesso geolocalizado

### Cloud CDN

+ CDN da GCloud
+ 100+ endpoints, sem necessidade de gerenciar configurações por região


### Cloud Interconnect
+ Conjunto de serviços GCloud para conectar as redes existentes à Google Network
+ Modalidades:
    - Interconnect (RFC 1918) para conectar dispositivos à VPC
        * Nativo
        * Partner, caso não haja Interconnect na região
    - Peering
        * Útil caso seja necessário utilizar aplicações do Google Workspace quando conectado via provedores (partners) 
    - Google VPN services
        * Quando não há requerimentos em relação à bandwidth


> Para pesquisar:
>
> - Qual a diferença, na prática, entre peering e interconnect?
> Aqui um link que explica melhor a diferença entre as 2 modalidades: https://callcenterstudio.com/blog/google-cloud-connection-blogs/

