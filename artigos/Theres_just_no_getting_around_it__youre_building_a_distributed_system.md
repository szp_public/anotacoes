# There’s Just No Getting around It: You’re Building a Distributed System

## Referência

1. https://queue.acm.org/detail.cfm?id=2482856

**Autor**: Mark Cavage

## Resumo

O autor diz que sistemas distribuídos são difíceis de entender, desenhar, construir e operar, pois introduz novas preocupações que não existiam no desenvolvimento "single machine". O autor também menciona que um sistema sem SLA útil que possa tolerar um downtime estendido ou problemas de degradação, tem uma enorme barreira de entrada no mundo distribuído. Outro aspecto importante é que quanto mais noves (9) acrescentados nas metas de disponibilidade, mais difícil de implantar se torna o sistema.

O autor também menciona que é extremamente comum haver falhas e degradações que se manifestam de forma intermitente, chamadas de *brownouts*. Ele exemplifica o caso da empresa Joyent que teve um problema de locking nos processos internos de uma aplicação NodeJS que foi muito difícil de achar a causa raiz.

Mas por que migrar para a nuvem? O autor cita as seguintes tendências de migrar para uma arquitetura distribuída:
1. Crescente demanda por consumir API's que excedem, e muito, as capacidades de uma máquina única;
2. Uma empresa move sua aplicação existente para o ambiente de Nuvem para diminuir os custos de hardware/data-center.

Para piorar, os sistemas distribuídos de sucesso foram construídos sob extrema tutelagem acadêmica ou no 'vai ou racha'. Além disto, a literatura foca em tecnologias em vez da técnica, o que deixa vários problemas não resolvidos, pois estas não resolvem todos os problemas.


**Construindo um sistema distribuído**


Em uma moderna arquitetura SOA, os serviços tem os seguintes predicados: são discretos, independentes e escaláveis. Para cada serviço devem ser considerados:
1. Geografia: Será global? Regional? Executar somente em um "Silo"?
2. Segregação de dados: Será multitenancy ou single tenant?
3. SLA: Metas de disponibilidade, latência, throughput, consistência e durabilidade devem ser definidas;
4. Segurança: Identidade, autenticação, autorização, auditoria;
5. Rastreamento de uso: Serve tanto para determinar as capacidades mínimas necessárias para execução do serviço, quanto para aspectos de governança, billing e determinar comportamento dos usuários/clientes;
6. Deployment e gerenciamento de configurações: Como serão realizados os updates da aplicação?

O autor ainda exemplifica, que por menor que a aplicação seja, ainda existem aspectos que precisam ser avaliados, mesmo que seja uma aplicação sem estado. Por exemplo, qual o  numero máximo de requisições o serviço suportaria em uma única região, qual a meta de disponibilidade e latência.

No artigo  autor constrói um serviço de processamento de imagens que possui as seguintes partes/módulos:
1. Uma API para receber e responder às chamadas;
2. Um sistema autoritativo de gerenciamento de identidade;
3. Um broker de mensagens (queue);
4. Um serviço escalável de processamento de imagens;
5. Um serviço de agregação de uso para operações, cobrança, etc.

Para cada parte do serviço, o autor menciona que é preciso avaliar os 6 pontos mencionados acima, levando em consideração que o item 3 (SLA) é subdividido em 3 acordos, observando os índices de cumprimento:
a. Acordo de throughput mínimo suportado (ex. 5000 req/s);
b. Acordo de latência máxima, neste caso 99% das requisições devem estar abaixo da latência máxima;
c. Acordo de disponibilidade mínimo;

Em relação ao sistema de mensageria, é importante ressaltar que é necessário que haja um mecanismo de "Automatic failover", pois se um ou mais nós do broker de mensagens estiver offline, o sistema de failover entra em ação, evitando a perda de mensagens. Isto adiciona uma densa camada de complexidade, pois é preciso considerar o modelo de replicação mais eficiente para o serviço desejado.

É importante também observar que, cada módulo possui características distintas a serem consideradas, por exemplo, o serviço de autenticação/identidade é do tipo read-intensive, enquanto o serviço de captura de uso é write-intensive. Isto exige um tratamento e definição de estratégias diferentes para cada módulo da aplicação.

Resumindo, o autor dita os seguintes pontos para construção da arquitetura:
- Decompor a aplicação de negócio em serviços discretos considerando a tolerância a falhas, escalabilidade e "data workloads" (read/writes);
- Fazer os serviços o mais stateless possível;
- Ao lidar com estado, entenda profundamente o teorema CAP, a latência, o throughput e os requisitos de durabilidade;

O restante do artigo irá tratar do ciclo de vida da aplicação, que não estará neste resumo;



## Outros pontos importantes

- Utilizar softwares open-source não é uma decisão sábia para sistemas de produção. Principalmente porque as premissas que escreveram a ferramenta podem não estar alinhadas com as da equipe que irá implantá-la;
- O processo de validação e testes em sistemas distribuídos é bem complexo;
- A operação (Operations) de um sistema distribuído é complexa, isto significa que cada software precisa ter membros da equipe de infra/ops que o conheça a fundo, além de realizar operação de monitoramento, aplicação de patches e configuração (sustentação);
