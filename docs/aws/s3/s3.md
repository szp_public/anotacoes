# AWS S3


## Definição
O S3 é um serviço de armazenamento de objetos que provê:
- Durabilidade (acima de 99.9%)
- Disponilibidade (Geralmente 99.999999999%)
- Segurança

É possível armazenar praticamente qualquer tipo de informação.

## Recursos
### Classes de armazenamento

Os dados armazenados possuem classes de armazenamento que permitem um uso mais eficiente dos pontos de vista econômico, velocidade de desempenho e durabilidade. Com isso é possível definir as seguintes estratégias, por exemplo:
- Armazenar dados que tem acesso pouco frequente em classes mais econômicas
- Arquivar dados em classes que tem custos ainda menores que as outras padrão
- Utilizar um serviço de autoclassificação de classes de armazenamento (S3 Intelligent-Tiering), baseado nos padrões de acesso;

Os tipos de classe de armazenamento estão melhor descritos neste documento: https://docs.aws.amazon.com/pt_br/AmazonS3/latest/userguide/storage-class-intro.html

### Gerenciamento de armazenamento

É possível gerenciar:
- Ciclo de vida dos objetos - políticas de  transição de classes, expiração;
- Bloqueio de objetos - Políticas WORM (write once read many), proteção contra alteração/exclusão
- Replicação - replicação para outros buckets
- Operações batch - operações **copy**, **Invoke Lambda function** e **Restore**


### Gerenciamento de acesso e segurança

- Políticas de controle de acesso baseados na IAM
    +  Bloqueio de acesso público habilitado por padrão
    + Endpoints nomeados com políticas específicas 
    + Possibilidde de uso de ACL's para controlar acesso
    +  
- Auditoria de uso e acesso

### Processamento de dados

- S3 Object Lambda - Possível modificar e processar dados de acordo com o acesso via API. Ex.: É possível incluir marca d'água em um documento PDF, ou filtrar linhas de um CSV. Todo esse processo é executado por funções Lambda
- Notificações de eventos via Amazon SNS/SQS quando um recurso for alterado

### Registro e monitoramento do armazenamento

- Automatizado
    + Métricas do CloudWatch - Alertas de faturamento quando as cobranças atingirem um threshold
    + Registra ações de usuários, Functions ou Service no CloudTrail
- Manual
    + Log de acesso ao servidor, em arquivos de log com formato específico
    + Sujeito ao AWSTrusted Advisor, que é um serviço que verifica se os serviços estão utilizado práticas recomendadas, além de identificar e recomendar melhores formas de uso


### Análise e insights

- Análise de classe - fornece análise de uso para tomadas de decisão sobre necessidade ou não de movimentação para outras classes mais apropriadas
- S3 Storage Lens - Gráficos e visibilidade baseado em métricas coletadas pelo serviço
- Inventário - Auditoria e relatórios para ajudar no gerenciamento. Este relatório é configurável  e agendável

### Consistência

- Não utiliza o modelo RYOW (read your ownn writes), ou seja, após um PUT, é possível que outra thread retorne os dados antigos
- O modelo de concorrência de escritas é o LWW (last write wins)

## Como funciona?

> **Definições**
>
> - Partições: Um agrupamento de regiões, ex.: `aws-`, `aws-cn`, `aws-us-gov`
>
> - Objetos: Arquivo + metadados
>
> - Buckets: Contêiner de armazenamento de objetos.  
>   + É restrito à região onde fora criado, não sendo possível trocá-lo de região.
>   + Somente 100 buckets por conta, por padrão.
>   + É possível fazer upload via API, bem como console AWS
>   + O nome do bucket é exclusivo em cada conta AWS.
>
> - Chaves: 
>   + Identifica um objeto num bucket
>   + A combinação "bucket + chave + versao" identificam um objeto exclusivo
> 
> - Versionamento: Serve para manter várias versões do mesmo objeto.
>   + ID da versão: identificador exclusivo de versão de cada objeto no bucket 
> 
> - Política de bucket: Mecanismo de concessão de acesso
>   + Baseado em no IAM
>   + Somente o proprietário pode associar política 
>   + Políticas são baseadas em JSON e tem limite de 20KB
>
> - Pontos de acesso (Access Points)
>   + São uma espécie de políticas de acesso dos dados do bucket. Por exemplo, é possível definir uma política que limite o acesso proveniente de uma VPC específica, 
> enquanto outras VPCs podem ter outras políticas de  acesso
>
> - ACLs: 
    + É possível conceder permissões específicas em objetos e buckets
    + A documentação recomenda deixar as ACLs desabilitadas, a não ser que haja necessidade de controle granular de acesso (gerenciamento trabalhoso)

Fluxo:
- Cria-se o bucket em uma região
- Upload dos dados para o bucket criado. 
- É possível definir, por exemplo, política de versionamento dos objetos

> **Notas**
> 
> - Assim, cada objeto possui uma chave/identificador exclusivo no bucket;
>
> - Um bucket e seus objetos são privados por padrão. Sendo necessário configuração explícita como os objetos serão acessados
> - Gerenciamento de acesso por: IAM, ACL's, políticas do bucket, pontos de acesso do S3

## Acesso

